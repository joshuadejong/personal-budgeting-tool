from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import View, DetailView, CreateView, ListView, UpdateView

from .models import Transaction, Account
from .forms import TransactionFormSet, AccountForm


class AccountCreateView(CreateView):

    model = Account
    form_class = AccountForm
    success_url = 'empty'
    template_name = 'account_create'

    def get(self, request, *args, **kwargs):
        """Instantiates a blank form for creating accounts with inline
        form for adding transactions.
        """
        self.object = None

        form_class = self.get_form_class()
        form = self.get_form(form_class)
        transaction_form = TransactionFormSet()

        return self.render_to_response(
            self.get_context_data(form=form,
                                  transaction_form=transaction_form)
        )

    def post(self, request, *args, **kwargs):
        """Populates Account and inline Transaction forms with POST
        data.
        """

        self.object = None

        form_class = self.get_form_class()
        form = self.get_form(form_class)
        transaction_form = TransactionFormSet(self.request.POST)

        if form.is_valid() and transaction_form.is_valid():
            return self.form_valid(form, transaction_form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form, transaction_form):
        """Instantiates a new account object and redirects to
        the success_url.
        """

        self.object = form.save()
        transaction_form.instance = self.object
        transaction_form.save()

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, transaction_form):
        """Returns the invalid form with rendered with errors"""

        return self.render_to_response(
            self.get_context_data(form=form,
                                  transaction_form=transaction_form)
        )