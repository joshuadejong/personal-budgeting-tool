# coding=utf-8
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Transaction(models.Model):
    """The base abstract class to represent credit and debit transactions."""

    DEBIT = '0'
    CREDIT = '1'

    TRANSACTION_TYPES = (
        (DEBIT, 'Debit'),
        (CREDIT, 'Credit'),
    )

    account = models.ForeignKey(Account)
    transaction_type = models.CharField(max_length=1, choices=TRANSACTION_TYPES)
    # assumed that transactions will not exceed ±9,999,999
    value = models.DecimalField(max_digits=9, decimal_places=2)
    date = models.DateField(auto_now_add=True)


class Account(models.Model):
    """Account is a conceptual container where transactions take place over
    a period of time. Examples include budget categories, but could include
    chequing and savings accounts.
    """

    # Accounts can be of many possible types
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    user = models.ForeignKey()
    budget = models.DecimalField(max_digits=9, decimal_places=2)
    start = models.DateField()
    end = models.DateField()
    created = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    @property
    def net(self):
        transactions = Transaction.objects.filter(account=self)
        total_net = 0.0
        for transaction in transactions:
            if transaction.transaction_type == '0':
                total_net -= transaction.value
            else:
                total_net += transaction.value
        return total_net