from django.forms import ModelForm
from django.forms.models import inlineformset_factory

from .models import Account, Transaction


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ['budget', 'start', 'end']


class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['transaction_type', 'value']

TransactionFormSet = inlineformset_factory(Account, Transaction)